package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	service := gin.Default()

	service.GET("/v1/status", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "ok"})
	})

	service.GET("/v1/register", func(c *gin.Context) {
		msgID := c.Query("msg_id")
		status := c.Query("message_status")
		c.JSON(http.StatusOK, gin.H{"message": fmt.Sprintf("%s: %s", msgID, status)})
	})
	service.OPTIONS("/dlr/v1/register", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
		c.JSON(http.StatusOK, struct{}{})
	})

	service.Run(port())
}

func port() string {
	port := os.Getenv("DLR_PORT")
	if len(port) == 0 {
		port = "6070"
	}
	return ":" + port
}
