# example-dlr

This is a PoC for an SMS DLR cloud native Microservice.


## Quick Start

1. Clone the repo
```
git clone https://gitlab.com/cardiffnetworks/example-dlr.git
```

2. Then change to dir
```
cd example-dlr
```

3. Build and run service locally
```
docker-compose down && docker-compose build && docker-compose up
```

4. Access service
- Status Endpoint
```
GET http://localhost:6677/fdi/dlr/status 
```

- Register DLR Endpoint
```
GET http://localhost:6677/fdi/dlr/register?msg_id=abc&message_status=DELVD
```

### CLOUD NATIVE INFRASTRUCTURE

- Kubernetes Orchestration
k8s manifests available on demand

- Terraform Infrastructure provisioning
Configurations available on demand depending on your cloud infrastructure provider where you want to deploy this test service
