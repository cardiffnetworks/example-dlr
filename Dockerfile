FROM golang:alpine as builder
RUN apk update && apk add git 
COPY . $GOPATH/src/gitlab.com/example-dlr/
WORKDIR $GOPATH/src/gitlab.com/example-dlr/
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /go/bin/example-dlr .

FROM scratch
COPY --from=builder /go/bin/example-dlr /go/bin/example-dlr
COPY --from=builder  /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["/go/bin/example-dlr"]

